import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json
import matplotlib.pyplot as plt

# Método para abrir archivos json
def open_file():
	try:
		with open('inventario.json', 'r') as file:
			data = json.load(file)
		file.close()
	except IOError:
		data = []

	return data

# Método para guardar archivos json
def save_file(data):
    print("Save File")

    # Guardamos todos los datos enviados en 'data' en formato Json
    with open('inventario.json', 'w') as file:
        json.dump(data, file, indent=4)
    file.close()

def grafico(nombre, x, y):
    plt.bar(x, y, color=(0,1,0), align='center')
    plt.ylabel("Valores")
    plt.title('Estadisticas de '+ nombre)
    plt.suptitle(nombre)
    plt.show()

# Clase para ejecutar el menú inicial
class MenuInicial():

    def __init__(self):
        print("Constructor!")
        # Se crea un objeto para manipular Gtk
        self.builder = Gtk.Builder()
        # Se agregan objetos desde Ventux.glade
        self.builder.add_from_file("VentuxFinal.glade")
        self.window = self.builder.get_object("menuInicial")
        self.window.connect("destroy", Gtk.main_quit)
        # Tamaño de la ventana inicial
        self.window.set_default_size(800, 400)
        # Titulo de la ventana inicial
        self.window.set_title("Ventux")

        # Botones
        # Boton para ingresar como administrador
        self.BotonAdmin = self.builder.get_object("botonIngresarAdmi")
        # Boton para revisar el 'Acerca de'
        self.BotonAcercade = self.builder.get_object("botonAcercaDe")
        # Boton para ingresar como cliente (proximamente)
        self.BotonCliente = self.builder.get_object("botonIngresarCliente")
        # Mostrar todos los componentes de la ventana
        self.window.show_all()
        # Activacion de los botones
        self.BotonAdmin.connect("clicked", self.BotonAdmi)
        # self.BotonAcercade.connect("clicked", self.BotonAcercaDe)
        self.BotonCliente.connect("clicked", self.Boton_Cliente)
    
    # Método para ingresar como Administrador
    def BotonAdmi(self, btn=None):

    	aux = ConfirmarContraseñaAdmi()
    	print("\nDecidiste ingresar como Administrador.")

    """
    # Método para abrir 'Acerca de'
    def BotonAcercaDe(self, btn=None):

    	aux = About()
    	print("\nPresionaste el boton 'Acerca de'.")
    """
    # Método para ingresar como cliente
    def Boton_Cliente(self, btn=None):

    	aux = ClienteProx()
    	print("\nProximamente...")

# Clase para confirmar la contraseña de Administrador
class ConfirmarContraseñaAdmi():

	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("VentuxFinal.glade")
		self.ventanacontra = self.builder.get_object("confirmarContraseñaAdmi")
		self.ventanacontra.set_default_size(500, 150)
		self.ventanacontra.set_title("Confirmar contraseña")
		
		self.BotonConfirmarContraseña = self.builder.get_object("botonConfirmarIngresoAdmi")
		self.BotonConfirmarContraseña.connect("clicked", self.entrar)

		self.BotonCancelarContraseña = self.builder.get_object("botonCancelarIngresoAdmi")
		self.BotonCancelarContraseña.connect("clicked", self.cancelar)

		self.EntradaContraseña = self.builder.get_object("entradaContraseña")

		self.ventanacontra.show_all()
        
	def entrar(self, btn=None):

		ContraseñaParaIniciar = "1234"

		contraseña_Ingresada = self.EntradaContraseña.get_text()

		print("\nCONTRASEÑA INGRESADA: ", contraseña_Ingresada)


		if (ContraseñaParaIniciar == contraseña_Ingresada):

			aux = InventarioAdmin()
			print("¡Bienvenido nuevamente!")
			self.ventanacontra.destroy()

		elif(ContraseñaParaIniciar != contraseña_Ingresada):

			print("Contraseña Incorrecta")

			self.ventanacontra.destroy()

	def cancelar(self, btn=None):

		print("Cancelando inicio...")
		self.ventanacontra.destroy()

# Clase para ver panel de administración del inventario digital
class InventarioAdmin():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("VentuxFinal.glade")
		self.v_admin = self.builder.get_object("ventanaPrincipalAdmi")
		self.v_admin.set_default_size(600, 300)
		self.v_admin.set_title("Inventario Digital")

		self.BotonAgregarProducto = self.builder.get_object("botonAdd")
		self.BotonEliminarProducto = self.builder.get_object("botonElim")
		self.BotonRevisarInventario = self.builder.get_object("botonRevInv")
		self.BotonEstadistica = self.builder.get_object("botonStats")
		self.cerrartodo = self.builder.get_object("cerrarSesion")

		self.v_admin.show_all()

		self.BotonAgregarProducto.connect("clicked", self.Añadir)
		self.BotonEliminarProducto.connect("clicked", self.Eliminar)
		self.BotonRevisarInventario.connect("clicked", self.RevisarInv)
		self.BotonEstadistica.connect("clicked", self.Estadisticas)
		self.cerrartodo.connect("clicked", self.cerrarusuario)

	def cerrarusuario(self, btn=None):
		print("Cerrando sesión...")
		self.v_admin.destroy()
		aux = dialogofinal()

	def Añadir(self, btn=None):

		print("Agregando...")
		auxiliar = Añadir_Producto()

	def Eliminar(self, btn=None):

		print("Eliminando...")
		auxiliar = Eliminar_Producto()

	def RevisarInv(self, btn=None):

		print("Revisando inventario...")
		auxiliar = Revisar_Inventario()

	def Estadisticas(self, btn=None):

		print("Revisando estadísticas...")
		auxiliar = Revisar_Stats()

class dialogofinal():

	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("VentuxFinal.glade")
		self.dialogo = self.builder.get_object("dialogoSesionCerrada")
		self.dialogo.set_default_size(350, 100)
		self.dialogo.set_title("Has cerrado sesión correctamente")

		self.botonCERRAR = self.builder.get_object("cerrando")
		self.botonCERRAR.connect("clicked", self.adios)

		self.dialogo.show_all()

	def adios(self, btn=None):

		print("Cerrando dialogo...")
		self.dialogo.destroy()

# Clase para ingresar como cliente (proximamente)
class ClienteProx():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("VentuxFinal.glade")
		prox = self.builder.get_object("clientee")
		prox.set_default_size(350, 100)
		prox.set_title("PRONTO")

		prox.show_all()

class Revisar_Stats():

	def __init__(self):
		print("PROBANDO :D")
		#self.builder.Gtk.Builder()
		"""
		columnas = ['pan', 'bebida']
		valores = [1, 10]

		plt.figure(1, figsize = (9,3))

		plt.subplot(131)
		plt.plot(columnas, valores)
		plt.suptittle('PRUEBA')
		plt.show()
		"""
		model, it = self.treeResultado.get_selection().get_selected()
		if model is None or it is None:
			return

		nombre = model.get_value(it, 0)
		victorias = model.get_value(it, 4)
		empates = model.get_value(it, 6)
		derrotas = model.get_value(it, 5)


		y = [str(0), victorias, empates, derrotas]
		x = ["", "Victorias", "Empates", "Derrotas"]

		grafico(nombre, x, y)

# Clase para añadir producto desde la administración
class Añadir_Producto():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("VentuxFinal.glade")
		self.añadir_p = self.builder.get_object("ventanaAgregarProducto")
		self.añadir_p.set_default_size(350, 200)
		self.añadir_p.set_title("Añadir un producto")

		#self.cancelarPasillo = self.builder.get_object("cancelarPASILLO")
		#self.cancelarPasillo.connect("clicked", self.cerrarAñadir)

		self.aceptarañadir = self.builder.get_object("botonConfirmarAñadirProducto")
		self.aceptarañadir.connect("clicked", self.ListaProductosAñadir)

		self.cancelarañadir = self.builder.get_object("botonCancelarAñadirProducto")
		self.cancelarañadir.connect("clicked", self.cerrarAñadir)

		self.añadir_p.show_all()
		
		self.produc = self.builder.get_object("entradaProductoAP")
		self.precio = self.builder.get_object("entradaPrecioAP")
		self.codigoo = self.builder.get_object("entradaCodigoAP")
		self.cantidad = self.builder.get_object("entradaCantidadAP")
		self.pasillo = self.builder.get_object("combobox2")

	def cerrarAñadir(self, btn=None):

		self.añadir_p.destroy()
		print("Cancelando...")

	def ListaProductosAñadir(self, btn=None):

		self.pasillo = self.builder.get_object("combobox2")
		pasillo_escogido = self.pasillo
		lista1 = self.produc.get_text()
		lista2 = self.precio.get_text()
		lista3 = self.codigoo.get_text()
		lista4 = self.cantidad.get_text()

		j = {"Pasillo": [{"Codigo": lista3, "Cantidad": lista4, "Precio": lista2, "Producto": lista1}]}

		f = open_file()

		f.append(j)
		save_file(f)
		self.añadir_p.destroy()
		print("Se ha registrado correctamente el producto.")

# Clase para eliminar producto desde la administración
class Eliminar_Producto():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("VentuxFinal.glade")
		eliminar_p = self.builder.get_object("ventanaEliminarProducto")
		eliminar_p.set_default_size(350, 200)
		eliminar_p.set_title("Eliminar un producto")

		eliminar_p.show_all()
# Clase para revisar el inventario general
class Revisar_Inventario():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("VentuxFinal.glade")
		rev_inv = self.builder.get_object("ventanaRevisarInventario")
		rev_inv.set_default_size(350, 200)
		rev_inv.set_title("Revisar Inventario")

		self.listmodel = Gtk.ListStore(str, str)
		self.treeResultado = self.builder.get_object("treeResultado")
		self.treeResultado.set_model(model=self.listmodel)

		with open("inventario.json","r") as file:
			data = json.load(file)

		file.close()

#		for leido_de_data in data['Pasillo']:


		cell = Gtk.CellRendererText()
		title = ("Producto", "Codigo", "Cantidad", "Precio")
		for i in range(len(title)):
			col = Gtk.TreeViewColumn(title[i], cell, text=i)
			self.treeResultado.append_column(col)
			print(data)

		#self.show_all_data()

		rev_inv.show_all()

	def show_all_data(self):

		data = open_file()

		print (data)

		for i in range (len(data)):
			x = data[i][str(1)][0].values()
			self.listmodel.append(x)

if __name__ == '__main__':
    MenuInicial()
    Gtk.main()